<?php
/**
 * The template for displaying 404 pages (not found).
 * @package understrap
 */

get_header(); ?>
<div class="404-wrapper">
    <div class="container">
        <div class="row">
            <main class="col-md-12" role="main">

                <section class="error-404 not-found">

                    <header class="page-header">
                        <h2 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'understrap' ); ?></h2>
                    </header><!-- .page-header -->

                    <div class="page-content">

                        <p><?php _e( 'It looks like a nothing was found at this location. Maybe try one of the links below or a search?', 'understrap' ); ?></p>

                        <?php get_search_form(); ?>

                    </div><!-- .page-content -->
                </section><!-- .error-404 -->
            </main><!-- #main -->
        </div> <!-- .row -->
    </div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
