<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}
?>

<div class="col-sm-4">
    <div id="secondary" class="widget-area right-widget-area" role="complementary">
        <?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div>
</div><!-- #secondary -->
