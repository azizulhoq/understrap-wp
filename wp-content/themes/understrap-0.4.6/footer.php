<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
?>

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class="powered-by">Lorem ipsum dolor sit amet.<a href="#" target="_blank">Powered by</a></p>
            </div>
            <div class="col-sm-6">
                <p class="copyright-info">Lorem ipsum dolor sit amet.<a href="#" target="_blank">Powered by</a></p>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->


<?php wp_footer(); ?>
</body>

</html>
