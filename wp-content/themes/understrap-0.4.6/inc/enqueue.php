<?php
/**
 * understrap enqueue scripts
 *
 * @package understrap
 */
define('S_DEV', true);
define('VERSION', '1');

function understrap_scripts() {
	wp_enqueue_style( 'font-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700');

     if(S_DEV) {
        //development files
        wp_enqueue_style( 'plugin', get_stylesheet_directory_uri() . '/css/plugin.css', array(), VERSION, 'all' );
        wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/css/theme-style.css', array(), VERSION, 'all' );

        wp_enqueue_script('jquery');
    	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main-script.js', array(), VERSION, true );

    } else {
        //production
        wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/css/style.min.css', array(), VERSION, 'all' );

        wp_enqueue_script('jquery');
        wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main-script.min.js', array(), VERSION, true );
    }

}

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );


// Removing the fuc!?$% emoji´s
// remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
// remove_action( 'wp_print_styles', 'print_emoji_styles' );
