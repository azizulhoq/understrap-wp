<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'understrap');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bR1n(Acdr}?%S;w)#MODTX>6h3fvv2PcAkotQ[3Lhz2i6^hvg$,ec1{5I7%mUvWz');
define('SECURE_AUTH_KEY',  '(YoQk|8:W#dX`ol&o:`aN98)1Hw(BNAs<yRH,hG={yBYk@T6LuTOZmQ^U2v}%N7u');
define('LOGGED_IN_KEY',    'iPRz50tDax8Y,E|NUh!T1:J#e(Ld&8mD?DuXpE/jU!X/eSt;|>i@u#l-M!a+t#>|');
define('NONCE_KEY',        'sh@RBQ#E[%]smjEne ;e<Owv=K)&0#-9L*xhqB:2/W/@LGF?b7&EiFpoW7Q&cjFH');
define('AUTH_SALT',        '_l8WCU}yc0YnC4{m|T6SM>Kk-8cB/k?W_rmHAn{Ci^BOjqM*vt|E5^YB-65p&ytb');
define('SECURE_AUTH_SALT', 'uYN`PRnu0:#jOEs49(UdJ47>n%^|B1M[*7c^)>s#e/z/Uxs(P{(=L,H?-9Lw@dg$');
define('LOGGED_IN_SALT',   'Q|{:DtQFQYdOR94OS9snYkUlW6`<&#;[H=aYBBwNR0z9I`[%uSz02o2$#BO,u9 -');
define('NONCE_SALT',       '+--Zh:|RtbaORF;Ahi~^X!HWrH$N)Epwebhp_F3{e+bo$T[FKL=XNr/^f0+,*@f&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'un_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
